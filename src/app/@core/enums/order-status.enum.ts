export enum OrderStatus {
  UNPAID = 'unpaid',
  PAID = 'paid',
  IN_DELIVERY = 'in_delivery',
  DELIVERED = 'delivered',
}
