export enum PaymentMethod {
  CARD = 'cart',
  ON_DELIVERY = 'on_delivery',
}
