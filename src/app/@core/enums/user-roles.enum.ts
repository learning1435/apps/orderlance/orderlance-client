export enum UserRoles {
  QUEST = 'guest',
  USER = 'user',
  ADMIN = 'admin',
}
