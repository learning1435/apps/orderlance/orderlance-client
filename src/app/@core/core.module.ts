import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NbAuthJWTToken, NbAuthModule, NbPasswordAuthStrategy} from '@nebular/auth';
import {NbSecurityModule, NbRoleProvider} from '@nebular/security';
import {of as observableOf} from 'rxjs';

import {
  UserData,
} from '../@services';
import {IsAuthenticated} from './guards/is-authorized.guard';
import {environment} from '../../environments/environment';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {UnauthorizedHandlerInterceptor} from './interceptors/unauthorized-handler.interceptor';

const SERVICES = [
  UserData,
];

export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}

export const NB_CORE_PROVIDERS = [
  ...NbAuthModule.forRoot({

    strategies: [
      NbPasswordAuthStrategy.setup({
        name: 'email',
        baseEndpoint: `${environment.baseUrl}/api/auth/`,
        token: {
          class: NbAuthJWTToken,
          key: 'token',
        },
        login: {
          redirect: {
            success: '/',
          },
        },
        register: {
          redirect: {
            success: '/auth/login',
          },
        },
        logout: {
          redirect: {
            success: '/auth/login',
            failure: '/auth/login',
          },
        },
      }),
    ],
    forms: {
      login: {},
      register: {},
    },
  }).providers,

  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*',
      },
    },
  }).providers,
  {
    provide: NbRoleProvider, useClass: NbSimpleRoleProvider,
  },
  IsAuthenticated,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: [],
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
        ...SERVICES,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: UnauthorizedHandlerInterceptor,
          multi: true,
        },
      ],
    };
  }
}
