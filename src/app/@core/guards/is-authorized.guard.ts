import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {NbAuthService} from '@nebular/auth';

@Injectable({providedIn: 'root'})
export class IsAuthenticated implements CanActivate {

  constructor(
    private authService: NbAuthService,
    private router: Router,
  ) {
  }

  public canActivate(): boolean {
    if (this.authService.isAuthenticated()) {
      return true;
    }

    this.router.navigate(['/auth', 'login']);
    return false;
  }
}
