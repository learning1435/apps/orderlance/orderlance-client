import {animate, animateChild, query, stagger, style, transition, trigger} from '@angular/animations';

export const LIST_ANIMATION = [
  trigger('list', [
    transition(':enter', [
      query('@items', stagger(200, animateChild())),
    ]),
  ]),
  trigger('items', [
    transition(':enter', [
      style({transform: 'scale(0.5)', opacity: 0}),
      animate('0.5s cubic-bezier(.8, -0.6, 0.2, 1.5)',
        style({transform: 'scale(1)', opacity: 1})),
    ]),
    transition(':leave', [
      style({transform: 'scale(1)', opacity: 1, height: '*'}),
      animate('0.5s cubic-bezier(.8, -0.6, 0.2, 1.5)',
        style({
          transform: 'scale(0.5)', opacity: 0,
          height: '0px', margin: '0px',
        })),
    ]),
  ]),
];
