import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  NbActionsModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSidebarModule,
  NbUserModule,
  NbContextMenuModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbThemeModule, NbInputModule, NbCardModule, NbAlertModule, NbSpinnerModule,
} from '@nebular/theme';

import {NbEvaIconsModule} from '@nebular/eva-icons';
import {NbSecurityModule} from '@nebular/security';

import {
  ChangePasswordComponent,
  ChangeProfileComponent,
  ConfirmDialogComponent,
  FooterComponent,
  HeaderComponent,
  MealCounterComponent,
  PaymentMethodComponent,
  CategoryFormComponent,
  MealFormComponent,
} from './components';

import {
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  SafeUrlPipe,
} from './pipes';

import {AdminLayoutComponent, OneColumnLayoutComponent} from './layouts';
import {DEFAULT_THEME} from './styles/theme.default';
import {COSMIC_THEME} from './styles/theme.cosmic';
import {CORPORATE_THEME} from './styles/theme.corporate';
import {DARK_THEME} from './styles/theme.dark';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const NB_MODULES = [
  NbLayoutModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbContextMenuModule,
  NbSecurityModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbEvaIconsModule,
];

const COMPONENTS = [
  HeaderComponent,
  FooterComponent,
  OneColumnLayoutComponent,
  AdminLayoutComponent,
  MealCounterComponent,
  PaymentMethodComponent,
  ConfirmDialogComponent,
  ChangePasswordComponent,
  ChangeProfileComponent,
  CategoryFormComponent,
  MealFormComponent,
];

const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  SafeUrlPipe,
];

@NgModule({
  imports: [
    CommonModule,
    ...NB_MODULES,
    FormsModule,
    NbInputModule,
    NbCardModule,
    ReactiveFormsModule,
    NbAlertModule,
    NbSpinnerModule,
  ],
  exports: [CommonModule, ...PIPES, ...COMPONENTS],
  declarations: [...COMPONENTS, ...PIPES],
  entryComponents: [
    PaymentMethodComponent,
    ConfirmDialogComponent,
    ChangePasswordComponent,
    ChangeProfileComponent,
    CategoryFormComponent,
    MealFormComponent,
  ],
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [
        ...NbThemeModule.forRoot(
          {
            name: 'default',
          },
          [DEFAULT_THEME, COSMIC_THEME, CORPORATE_THEME, DARK_THEME],
        ).providers,
      ],
    };
  }
}
