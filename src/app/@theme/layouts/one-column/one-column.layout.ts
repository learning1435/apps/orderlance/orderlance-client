import { Component } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './one-column.layout.html',
  styleUrls: ['./one-column.layout.scss'],
})
export class OneColumnLayoutComponent {}
