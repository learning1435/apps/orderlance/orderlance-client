import {Component, Input, OnInit} from '@angular/core';
import {UserData, UserService} from '../../../@services';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User, ChangeProfile} from '../../../@models';
import {NbToastrService} from '@nebular/theme';

@Component({
  selector: 'app-change-profile',
  templateUrl: './change-profile.component.html',
  styleUrls: ['./change-profile.component.scss'],
})
export class ChangeProfileComponent implements OnInit {

  @Input() closeFn: Function;

  public form: FormGroup;
  public selectedAvatar: string = 'avatar-0';
  public avatars: Array<{ path: string, name: string }> = [];
  public loading: boolean = false;

  constructor(
    private userService: UserService,
    private userData: UserData,
    private toastrService: NbToastrService,
  ) {
    this.form = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(30),
        Validators.pattern('^[A-Za-z]+$'),
      ]),
    });

    for (let index: number = 0; index < 48; index++) {
      this.avatars.push({
        path: `/assets/images/avatars/avatar-${index}.svg`,
        name: `avatar-${index}`,
      });
    }
  }

  public ngOnInit() {
    this.userData
      .getUser()
      .subscribe((user: User) => {
        this.form.controls.username.setValue(user.username);
        this.selectedAvatar = user.avatar;
      });
  }

  public selectAvatar(avatar: string): void {
    this.selectedAvatar = avatar;
  }

  public save(): void {
    const requestData: ChangeProfile = {
      username: this.form.controls.username.value,
      avatar: this.selectedAvatar,
    };

    this.userService
      .changeProfile(requestData)
      .subscribe(
        () => {
          this.toastrService.success('Profile updated successfully', 'Success!');
          this.closeFn();
        },
        () => {
          this.toastrService.danger('An unexpected error has occurred!', 'Error!');
          this.closeFn();
        },
      );

    this.closeFn();
  }

  public cancel(): void {
    this.closeFn();
  }

  public hasError(control: string): boolean {
    return this.form.touched &&
      this.form.controls[control].touched &&
      this.form.controls[control].errors !== null;
  }
}
