import {Component, Input} from '@angular/core';
import {PaymentMethod} from '../../../@core/enums';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss'],
})
export class PaymentMethodComponent {

  @Input() confirmFn: Function;
  @Input() cancelFn: Function;

  public selectedMethod: PaymentMethod = null;
  public PaymentMethod: typeof PaymentMethod = PaymentMethod;

  public selectMethod(method: PaymentMethod): void {
    this.selectedMethod = method;
  }

  public confirm(): void {
    this.confirmFn(this.selectedMethod);
  }

  public cancel(): void {
    this.cancelFn();
  }
}
