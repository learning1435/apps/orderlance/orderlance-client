import {Component, Input, OnInit} from '@angular/core';
import {Category, Collection, CreateMeal, Meal, UpdateMeal} from '../../../@models';
import {CategoriesService, MealsService} from '../../../@services';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NbToastrService} from '@nebular/theme';

@Component({
  selector: 'app-meal-form',
  templateUrl: './meal-form.component.html',
  styleUrls: ['./meal-form.component.scss'],
})
export class MealFormComponent implements OnInit {

  @Input() confirmFn: Function;
  @Input() cancelFn: Function;
  @Input() meal: Meal = null;

  public form: FormGroup;
  public editing: boolean = false;
  public loading: boolean = false;
  public categoryOptions: Category[];

  constructor(
    private mealsService: MealsService,
    private toastrService: NbToastrService,
    private categoriesService: CategoriesService,
  ) {
    const reg: string = '(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?';
    this.form = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50),
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(200),
      ]),
      price: new FormControl('', [
        Validators.required,
        Validators.min(1),
        Validators.max(100),
      ]),
      categoryId: new FormControl('', [
        Validators.required,
      ]),
      imgUrl: new FormControl('', [
        Validators.required,
        Validators.pattern(reg),
      ]),
    });
  }

  public ngOnInit() {
    this.editing = this.meal !== null;

    if (this.editing) {
      this.form.setValue({
        name: this.meal.name,
        description: this.meal.description,
        imgUrl: this.meal.imgUrl,
        price: this.meal.price,
        categoryId: this.meal.categoryId,
      }, {emitEvent: true});
    }

    this.categoriesService
      .getCategories({page: 1, itemsPerPage: 100})
      .subscribe(
        (categories: Collection<Category>) => {
          this.categoryOptions = categories.members;

          if (this.editing) {
            this.form.controls.categoryId.setValue(this.meal.categoryId, {emitEvent: true});
          }
        },
      );
  }

  public save(): void {
    this.loading = true;
    if (this.editing) {
      this.update();
    } else {
      this.store();
    }
  }

  public cancel(): void {
    this.cancelFn();
  }

  public hasError(control: string): boolean {
    return this.form.touched &&
      this.form.controls[control].touched &&
      this.form.controls[control].errors !== null;
  }

  private store(): void {
    const data: CreateMeal = this.form.value;

    this.mealsService
      .storeMeal(data)
      .subscribe(
        () => {
          this.loading = false;
          this.showToastr('New category created successfully');
          this.confirmFn();
        },
      );
  }

  private update(): void {
    const data: UpdateMeal = this.form.value;
    data.id = this.meal.id;

    this.mealsService
      .updateMeal(data)
      .subscribe(
        () => {
          this.loading = false;
          this.showToastr('Category updated successfully');
          this.confirmFn();
        },
      );
  }

  private showToastr(message: string): void {
    this.toastrService.success(message, 'Success');
  }
}
