import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
})
export class ConfirmDialogComponent {

  @Input() title: string = 'Confirm';
  @Input() message: string = 'Do you want to perform this action?';
  @Input() confirmLabel: string = 'Yes';
  @Input() cancelLabel: string = 'No';
  @Input() confirmFn: Function;
  @Input() cancelFn: Function;

  public confirm(): void {
    this.confirmFn();
  }

  public cancel(): void {
    this.cancelFn();
  }
}
