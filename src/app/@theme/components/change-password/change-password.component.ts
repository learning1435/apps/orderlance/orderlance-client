import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent {

  @Input() closeFn: Function;

  public save(): void {
    this.closeFn();
  }

  public cancel(): void {
    this.closeFn();
  }
}
