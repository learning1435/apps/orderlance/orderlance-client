import {Component, OnDestroy, OnInit} from '@angular/core';
import {NbMediaBreakpointsService, NbMenuItem, NbMenuService, NbSidebarService, NbThemeService} from '@nebular/theme';
import {map, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {ShoppingCartService, UserData} from '../../../@services';
import {CartItem, User} from '../../../@models';
import {Router} from '@angular/router';
import {UserRoles} from '../../../@core/enums';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  public userPictureOnly: boolean = false;
  public user: User;
  public cartItems$: Observable<CartItem[]>;

  private destroy$: Subject<void> = new Subject<void>();

  public userMenu: NbMenuItem[] = [
    {
      title: 'Profile',
      link: '/profile',
    }, {
      title: 'Orders',
      link: '/orders',
    }, {
      title: 'Log out',
      link: '/auth/logout',
    }];

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private userService: UserData,
    private breakpointService: NbMediaBreakpointsService,
    private shoppingCartService: ShoppingCartService,
    private router: Router,
  ) {
    this.cartItems$ = this.shoppingCartService.cartItems$;
  }

  public ngOnInit(): void {
    this.userService.getUser()
      .pipe(takeUntil(this.destroy$))
      .subscribe((user: User) => {
        this.user = user;
        if (user.role === UserRoles.ADMIN) {
          this.userMenu = [...[{title: 'Dashboard', link: '/admin'}], ...this.userMenu];
        }
      });

    const {xl} = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public goToShoppingCart(): void {
    this.router.navigate(['/cart']);
  }

  public navigateHome(): void {
    this.router.navigate(['/']);
  }

  public getAvatar(): string {
    return `/assets/images/avatars/${this.user.avatar}.svg`;
  }
}
