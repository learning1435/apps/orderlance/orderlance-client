import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-meal-counter',
  templateUrl: './meal-counter.component.html',
  styleUrls: ['./meal-counter.component.scss'],
})
export class MealCounterComponent {

  @Input() count: number;
  @Output() countChanged: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  public get canIncrement(): boolean {
    return !(this.count >= 20);
  }

  public get canDecrement(): boolean {
    return !(this.count < 1);
  }

  public increment(): void {
    if (this.count >= 20) return;

    this.count++;
    this.countChanged.next(this.count);
  }

  public decrement(): void {
    if (this.count < 1) return;

    this.count--;
    this.countChanged.next(this.count);
  }
}
