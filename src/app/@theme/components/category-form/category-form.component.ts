import {Component, Input, OnInit} from '@angular/core';
import {CategoriesService} from '../../../@services';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Category, CreateCategory, UpdateCategory} from '../../../@models';
import {NbToastrService} from '@nebular/theme';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss'],
})
export class CategoryFormComponent implements OnInit {

  @Input() confirmFn: Function;
  @Input() cancelFn: Function;
  @Input() category: Category = null;

  public form: FormGroup;
  public editing: boolean = false;
  public loading: boolean = false;

  constructor(
    private categoriesService: CategoriesService,
    private toastrService: NbToastrService,
  ) {
    const reg: string = '(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?';
    this.form = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50),
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(200),
      ]),
      imgUrl: new FormControl('', [
        Validators.required,
        Validators.pattern(reg),
      ]),
    });
  }

  public ngOnInit() {
    this.editing = this.category !== null;

    if (this.editing) {
      this.form.setValue({
        title: this.category.title,
        description: this.category.description,
        imgUrl: this.category.imgUrl,
      });
    }
  }

  public save(): void {
    this.loading = true;
    if (this.editing) {
      this.update();
    } else {
      this.store();
    }
  }

  public cancel(): void {
    this.cancelFn();
  }

  public hasError(control: string): boolean {
    return this.form.touched &&
      this.form.controls[control].touched &&
      this.form.controls[control].errors !== null;
  }

  private store(): void {
    const data: CreateCategory = this.form.value;

    this.categoriesService
      .storeCategory(data)
      .subscribe(
        () => {
          this.loading = false;
          this.showToastr('New category created successfully');
          this.confirmFn();
        },
      );
  }

  private update(): void {
    const data: UpdateCategory = this.form.value;
    data.id = this.category.id;

    this.categoriesService
      .updateCategory(data)
      .subscribe(
        () => {
          this.loading = false;
          this.showToastr('Category updated successfully');
          this.confirmFn();
        },
      );
  }

  private showToastr(message: string): void {
    this.toastrService.success(message, 'Success');
  }
}
