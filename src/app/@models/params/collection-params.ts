export interface CollectionParams {
  page: number;
  itemsPerPage: number;
}
