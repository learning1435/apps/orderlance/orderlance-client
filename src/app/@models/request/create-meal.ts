export interface CreateMeal {
  name: string;
  description: string;
  imgUrl: string;
  price: number;
  categoryId: string;
}
