import {Meal} from '../meal';
import {PaymentMethod} from '../../@core/enums';

export interface MakeOrder {
  meals: MakeOrderItem[];
  paymentMethod: PaymentMethod;
}

export interface MakeOrderItem {
  count: number;
  meal: Meal;
}
