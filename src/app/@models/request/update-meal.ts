export interface UpdateMeal {
  id: string;
  name: string;
  description: string;
  imgUrl: string;
  price: number;
  categoryId: string;
}
