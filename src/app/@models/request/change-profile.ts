export interface ChangeProfile {
  username: string;
  avatar: string;
}
