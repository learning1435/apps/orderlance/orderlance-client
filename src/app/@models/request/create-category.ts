export interface CreateCategory {
  title: string;
  description: string;
  imgUrl: string;
}
