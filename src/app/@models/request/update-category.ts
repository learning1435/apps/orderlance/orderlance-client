export interface UpdateCategory {
  id: string;
  title: string;
  description: string;
  imgUrl: string;
}
