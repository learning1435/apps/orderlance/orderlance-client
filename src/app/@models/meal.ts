export interface Meal {
  id: string;
  name: string;
  description: string;
  imgUrl: string;
  price: number;
  categoryId: string;
}
