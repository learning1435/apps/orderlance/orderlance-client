import {UserRoles} from '../@core/enums';

export interface User {
  id: string;
  username: string;
  email: string;
  role: UserRoles;
  avatar: string;
}
