export interface Collection<T> {
  page: number;
  members: T[];
  totalItems: number;
  itemsPerPage: number;
}
