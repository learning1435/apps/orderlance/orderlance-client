import {OrderStatus, PaymentMethod} from '../@core/enums';

export interface Order {
  id: string;
  paid: boolean;
  status: OrderStatus;
  orderedAt: string;
  paidAt: string;
  userId: string;
  paymentMethod: PaymentMethod;
  total: number;
  items: OrderItem[];
}

export interface OrderItem {
  price: number;
  count: number;
  name: string;
}
