import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../@models';
import {NbAuthService, NbAuthToken} from '@nebular/auth';
import {map} from 'rxjs/operators';

@Injectable()
export class UserData {

  constructor(
    private authToken: NbAuthService,
  ) {
  }

  public getUser(): Observable<User> {
    return this.authToken
      .getToken()
      .pipe(
        map((token: NbAuthToken) => {
          const payload: any = token.getPayload();
          const userData: any = JSON.parse(payload.UserData);

          return {
            id: userData._id,
            username: userData.Username,
            email: userData.Email,
            role: userData.Role,
            avatar: userData.Avatar,
          };
        }),
      );
  }
}
