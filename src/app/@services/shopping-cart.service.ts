import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {CartItem} from '../@models';

const CART_LOCAL_STORAGE_KEY: string = 'shopping_cart';

@Injectable()
export class ShoppingCartService {

  private readonly _cartItems$: BehaviorSubject<CartItem[]>;
  private cartItems: CartItem[] = [];

  constructor() {
    const rawCartContent: string = localStorage.getItem(CART_LOCAL_STORAGE_KEY);

    if (rawCartContent) {
      const cartContent: CartItem[] = JSON.parse(rawCartContent);
      this.cartItems = cartContent;
      this._cartItems$ = new BehaviorSubject<CartItem[]>(cartContent);

      return;
    }

    this._cartItems$ = new BehaviorSubject<CartItem[]>([]);
  }

  public get cartItems$(): Observable<CartItem[]> {
    return this._cartItems$.asObservable();
  }

  public getCartItems(): CartItem[] {
    return this.cartItems;
  }

  public getCountForMeal(mealId: string): number {
    const existing: CartItem = this.cartItems.find((item: CartItem) => item.meal.id === mealId);

    if (existing) {
      return existing.count;
    }

    return 0;
  }

  public addItems(cartItem: CartItem): void {
    const newItem: CartItem = Object.assign({}, cartItem);
    const existing: CartItem = this.cartItems.find((item: CartItem) => item.meal.id === newItem.meal.id);

    if (existing) {
      if (newItem.count === 0) {
        this.cartItems = this.cartItems.filter((item: CartItem) => item.meal.id !== newItem.meal.id);
        this.setState();
        return;
      }

      existing.count = newItem.count;

      this.setState();
      return;
    }

    if (newItem.count === 0) return;

    this.cartItems.push(newItem);
    this.setState();
  }

  public clear(): void {
    this.cartItems = [];
    this.setState();
  }

  private setState(): void {
    localStorage.setItem(CART_LOCAL_STORAGE_KEY, JSON.stringify(this.cartItems));
    this._cartItems$.next(this.cartItems);
  }
}
