import {Injectable} from '@angular/core';
import {HttpClientService} from './http-client.service';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {Collection, CollectionParams, MakeOrder, Order} from '../../@models';

@Injectable({providedIn: 'root'})
export class OrdersService {

  private readonly RESOURCE: string = '/api/orders';

  constructor(
    private httpClientService: HttpClientService,
  ) {
  }

  public getOrdersForUser(params: CollectionParams): Observable<Collection<Order>> {
    let httpParams: HttpParams = new HttpParams();
    httpParams = httpParams.append('page', <any>params.page);
    httpParams = httpParams.append('itemsPerPage', <any>params.itemsPerPage);

    return this.httpClientService.get(this.RESOURCE, httpParams);
  }

  public getOrders(params: CollectionParams): Observable<Collection<Order>> {
    let httpParams: HttpParams = new HttpParams();
    httpParams = httpParams.append('page', <any>params.page);
    httpParams = httpParams.append('itemsPerPage', <any>params.itemsPerPage);

    return this.httpClientService.get(this.RESOURCE, httpParams);
  }

  public makeOrder(request: MakeOrder): Observable<Order> {
    return this.httpClientService.post(this.RESOURCE, request);
  }
}
