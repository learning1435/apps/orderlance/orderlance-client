import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {NbAuthService} from '@nebular/auth';

@Injectable()
export class HttpClientService {

  private _token: string = null;
  private _baseUrl: string = environment.baseUrl;

  constructor(
    private _httpClient: HttpClient,
    private _nbAuthService: NbAuthService,
  ) {
    this._nbAuthService
      .getToken()
      .subscribe((token) => {
        this._token = token.toString();
      });

    this._nbAuthService
      .onTokenChange()
      .subscribe((token) => {
        this._token = token.toString();
      });
  }

  public get(url: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this._httpClient
      .get(this._prepareUrl(url), this._prepareOptions(params));
  }

  public post(url: string, data: any, params: HttpParams = new HttpParams()): Observable<any> {
    return this._httpClient
      .post(this._prepareUrl(url), data, this._prepareOptions(params));
  }

  public put(url: string, data: any, params: HttpParams = new HttpParams()): Observable<any> {
    return this._httpClient
      .put(this._prepareUrl(url), data, this._prepareOptions(params));
  }

  public delete(url: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this._httpClient
      .delete(this._prepareUrl(url), this._prepareOptions(params));
  }

  private _prepareUrl(url: string): string {
    return `${this._baseUrl}${url}`;
  }

  private _prepareOptions(params: HttpParams): object {
    let result: any = {};

    if (this._token !== null) {
      result = Object.assign(result, {
        headers: {
          Authorization: `Bearer ${this._token}`,
        },
      });
    }

    return Object.assign(result, {params: params});
  }
}
