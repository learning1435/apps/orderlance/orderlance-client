import {Injectable} from '@angular/core';
import {HttpClientService} from './http-client.service';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {Collection, CollectionParams, CreateMeal, Meal, UpdateMeal} from '../../@models';

@Injectable({providedIn: 'root'})
export class MealsService {

  private readonly RESOURCE: string = '/api/meals';

  constructor(
    private httpClientService: HttpClientService,
  ) {
  }

  public getMeals(params: CollectionParams, categoryId: string = null): Observable<Collection<Meal>> {
    let httpParams: HttpParams = new HttpParams();
    httpParams = httpParams.append('page', <any>params.page);
    httpParams = httpParams.append('itemsPerPage', <any>params.itemsPerPage);

    if (categoryId) {
      httpParams = httpParams.append('categoryId', categoryId);
    }

    return this.httpClientService.get(this.RESOURCE, httpParams);
  }

  public storeMeal(data: CreateMeal): Observable<Meal> {
    return this.httpClientService.post(this.RESOURCE, data);
  }

  public updateMeal(data: UpdateMeal): Observable<void> {
    return this.httpClientService.put(this.RESOURCE, data);
  }

  public removeMeal(id: string): Observable<void> {
    return this.httpClientService.delete(`${this.RESOURCE}/${id}`);
  }
}
