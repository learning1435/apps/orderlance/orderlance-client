import {Injectable} from '@angular/core';
import {HttpClientService} from './http-client.service';
import {Observable} from 'rxjs';
import {ChangePassword, ChangeProfile, Collection, CollectionParams, User} from '../../@models';
import {HttpParams} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class UserService {

  private readonly RESOURCE: string = '/api/user';

  constructor(
    private httpClientService: HttpClientService,
  ) {
  }

  public getUsers(params: CollectionParams): Observable<Collection<User>> {
    let httpParams: HttpParams = new HttpParams();
    httpParams = httpParams.append('page', <any>params.page);
    httpParams = httpParams.append('itemsPerPage', <any>params.itemsPerPage);

    return this.httpClientService.get(this.RESOURCE, httpParams);
  }

  public changeProfile(request: ChangeProfile): Observable<void> {
    return this.httpClientService.put(this.RESOURCE, request);
  }

  public changePassword(request: ChangePassword): Observable<void> {
    return this.httpClientService.put(`${this.RESOURCE}/password`, request);
  }
}
