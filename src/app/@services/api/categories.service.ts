import {Injectable} from '@angular/core';
import {HttpClientService} from './http-client.service';
import {Observable} from 'rxjs';
import {Category} from '../../@models';
import {
  UpdateCategory,
  Collection,
  CollectionParams,
  CreateCategory,
} from '../../@models';
import {HttpParams} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class CategoriesService {

  private readonly RESOURCE: string = '/api/categories';

  constructor(
    private httpClientService: HttpClientService,
  ) {
  }

  public getCategories(params: CollectionParams): Observable<Collection<Category>> {
    let httpParams: HttpParams = new HttpParams();
    httpParams = httpParams.append('page', <any>params.page);
    httpParams = httpParams.append('itemsPerPage', <any>params.itemsPerPage);

    return this.httpClientService.get(this.RESOURCE, httpParams);
  }

  public storeCategory(data: CreateCategory): Observable<Category> {
    return this.httpClientService.post(this.RESOURCE, data);
  }

  public updateCategory(data: UpdateCategory): Observable<void> {
    return this.httpClientService.put(this.RESOURCE, data);
  }

  public removeCategory(id: string): Observable<void> {
    return this.httpClientService.delete(`${this.RESOURCE}/${id}`);
  }
}
