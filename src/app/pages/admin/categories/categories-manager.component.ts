import {Component, OnInit} from '@angular/core';
import {Category, Collection, CollectionParams} from '../../../@models';
import {CategoriesService} from '../../../@services';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {CategoryFormComponent, ConfirmDialogComponent} from '../../../@theme/components';

@Component({
  selector: 'app-categories-manager',
  templateUrl: './categories-manager.component.html',
})
export class CategoriesManagerComponent implements OnInit {

  public items: Category[] = [];
  public isLoading: boolean = false;
  public totalItems: number = 0;
  public currentParams: CollectionParams = {
    page: 1,
    itemsPerPage: 20,
  };

  constructor(
    private categoriesService: CategoriesService,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
  ) {
  }

  public ngOnInit() {
    this.getItems();
  }

  public onPageChange(page: number): void {
    this.currentParams.page = page;
    this.getItems();
  }

  public add(): void {
    const ref: NbDialogRef<CategoryFormComponent> = this.dialogService.open(CategoryFormComponent, {
      context: {
        cancelFn: () => ref.close(),
        confirmFn: () => {
          ref.close();
          this.getItems();
        },
      },
    });
  }

  public edit(item: Category): void {
    const ref: NbDialogRef<CategoryFormComponent> = this.dialogService.open(CategoryFormComponent, {
      context: {
        category: item,
        cancelFn: () => ref.close(),
        confirmFn: () => {
          ref.close();
          this.getItems();
        },
      },
    });
  }

  public remove(id: string): void {
    const ref: NbDialogRef<ConfirmDialogComponent> = this.dialogService.open(ConfirmDialogComponent, {
      context: {
        message: 'Do you want to remove this category?',
        cancelFn: () => ref.close(),
        confirmFn: () => {
          this.categoriesService.removeCategory(id)
            .subscribe(
              () => {
                ref.close();
                this.toastrService.success('Category removed successfully!', 'Success');
                this.getItems();
              },
            );
        },
      },
    });
  }

  private getItems(): void {
    this.categoriesService
      .getCategories(this.currentParams)
      .subscribe(
        (items: Collection<Category>) => {
          this.totalItems = items.totalItems;
          this.items = items.members;
          this.isLoading = false;
        },
      );
  }
}
