import {Component, OnInit} from '@angular/core';
import {MealsService} from '../../../@services';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {MealFormComponent, ConfirmDialogComponent} from '../../../@theme/components';
import {Collection, CollectionParams, Meal} from '../../../@models';

@Component({
  selector: 'app-meals-manager',
  templateUrl: './meals-manager.component.html',
})
export class MealsManagerComponent implements OnInit {

  public items: Meal[] = [];
  public isLoading: boolean = false;
  public totalItems: number = 0;
  public currentParams: CollectionParams = {
    page: 1,
    itemsPerPage: 20,
  };

  constructor(
    private mealsService: MealsService,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
  ) {
  }

  public ngOnInit() {
    this.getItems();
  }

  public onPageChange(page: number): void {
    this.currentParams.page = page;
    this.getItems();
  }

  public add(): void {
    const ref: NbDialogRef<MealFormComponent> = this.dialogService.open(MealFormComponent, {
      context: {
        cancelFn: () => ref.close(),
        confirmFn: () => {
          ref.close();
          this.getItems();
        },
      },
    });
  }

  public edit(item: Meal): void {
    const ref: NbDialogRef<MealFormComponent> = this.dialogService.open(MealFormComponent, {
      context: {
        meal: item,
        cancelFn: () => ref.close(),
        confirmFn: () => {
          ref.close();
          this.getItems();
        },
      },
    });
  }

  public remove(id: string): void {
    const ref: NbDialogRef<ConfirmDialogComponent> = this.dialogService.open(ConfirmDialogComponent, {
      context: {
        message: 'Do you want to remove this meal?',
        cancelFn: () => ref.close(),
        confirmFn: () => {
          this.mealsService.removeMeal(id)
            .subscribe(
              () => {
                ref.close();
                this.toastrService.success('Meal removed successfully!', 'Success');
                this.getItems();
              },
            );
        },
      },
    });
  }

  private getItems(): void {
    this.mealsService
      .getMeals(this.currentParams)
      .subscribe(
        (items: Collection<Meal>) => {
          this.totalItems = items.totalItems;
          this.items = items.members;
          this.isLoading = false;
        },
      );
  }
}
