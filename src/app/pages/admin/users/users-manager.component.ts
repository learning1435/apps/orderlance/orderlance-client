import {Component, OnInit} from '@angular/core';
import {Collection, CollectionParams, User} from '../../../@models';
import {UserService} from '../../../@services';

@Component({
  selector: 'app-orders-manager',
  templateUrl: './users-manager.component.html',
})
export class UsersManagerComponent implements OnInit {

  public items: User[] = [];
  public isLoading: boolean = false;
  public totalItems: number = 0;
  public currentParams: CollectionParams = {
    page: 1,
    itemsPerPage: 20,
  };

  constructor(
    private userService: UserService,
  ) {
  }

  public ngOnInit() {
    this.getItems();
  }

  public onPageChange(page: number): void {
    this.currentParams.page = page;
    this.getItems();
  }

  private getItems(): void {
    this.userService
      .getUsers(this.currentParams)
      .subscribe(
        (items: Collection<User>) => {
          this.totalItems = items.totalItems;
          this.items = items.members;
          this.isLoading = false;
        },
      );
  }
}
