import {Component, OnInit} from '@angular/core';
import {Collection, CollectionParams, Order} from '../../../@models';
import {OrdersService} from '../../../@services';

@Component({
  selector: 'app-orders-manager',
  templateUrl: './orders-manager.component.html',
})
export class OrdersManagerComponent implements OnInit {

  public items: Order[] = [];
  public isLoading: boolean = false;
  public totalItems: number = 0;
  public currentParams: CollectionParams = {
    page: 1,
    itemsPerPage: 20,
  };

  constructor(
    private ordersService: OrdersService,
  ) {
  }

  public ngOnInit() {
    this.getItems();
  }

  public onPageChange(page: number): void {
    this.currentParams.page = page;
    this.getItems();
  }

  private getItems(): void {
    this.ordersService
      .getOrders(this.currentParams)
      .subscribe(
        (items: Collection<Order>) => {
          this.totalItems = items.totalItems;
          this.items = items.members;
          this.isLoading = false;
        },
      );
  }
}
