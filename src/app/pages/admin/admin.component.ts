import {Component} from '@angular/core';
import {NbMenuItem} from '@nebular/theme';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
})
export class AdminComponent {

  public adminMenu: NbMenuItem[] = [
    {
      title: 'Dashboard',
      icon: 'home-outline',
      link: '/admin',
    },
    {
      title: 'Categories',
      icon: 'bookmark-outline',
      link: '/admin/categories',
    },
    {
      title: 'Meals',
      icon: 'heart-outline',
      link: '/admin/meals',
    },
    {
      title: 'Orders',
      icon: 'car-outline',
      link: '/admin/orders',
    },
    {
      title: 'Users',
      icon: 'people-outline',
      link: '/admin/users',
    },
  ];
}
