import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {PagesComponent} from './pages.component';
import {FrontPageComponent} from './main/front-page/front-page.component';
import {ShoppingCartComponent} from './main/user/shopping-cart/shopping-cart.component';
import {AdminComponent} from './admin/admin.component';
import {DashboardComponent} from './admin/dashboard/dashboard.component';
import {CategoriesManagerComponent} from './admin/categories/categories-manager.component';
import {MealsManagerComponent} from './admin/meals/meals-manager.component';
import {OrdersManagerComponent} from './admin/orders/orders-manager.component';
import {UsersManagerComponent} from './admin/users/users-manager.component';
import {OrderProcessingComponent} from './main/order-processing/order-processing.component';
import {OrdersComponent} from './main/user/orders/orders.component';
import {ProfileComponent} from './main/user/profile/profile.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {path: '', component: FrontPageComponent},
      {path: 'profile', component: ProfileComponent},
      {path: 'orders', component: OrdersComponent},
      {path: 'order-processing/:id', component: OrderProcessingComponent},
      {path: 'cart', component: ShoppingCartComponent},
    ],
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {path: '', component: DashboardComponent},
      {path: 'categories', component: CategoriesManagerComponent},
      {path: 'meals', component: MealsManagerComponent},
      {path: 'orders', component: OrdersManagerComponent},
      {path: 'users', component: UsersManagerComponent},
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
