import {Component, OnInit} from '@angular/core';
import {OrdersService} from '../../../../@services';
import {CollectionParams, Order, Collection} from '../../../../@models';
import {LIST_ANIMATION} from '../../../../@theme/animations';
import {OrderStatus, PaymentMethod} from '../../../../@core/enums';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  animations: LIST_ANIMATION,
})
export class OrdersComponent implements OnInit {

  public orders: Order[] = [];
  public isLoading: boolean = true;
  public currentParams: CollectionParams = {
    page: 1,
    itemsPerPage: 10,
  };
  public totalItems: number = 0;
  public OrderStatus: typeof OrderStatus = OrderStatus;
  public PaymentMethod: typeof PaymentMethod = PaymentMethod;

  constructor(
    private ordersService: OrdersService,
  ) {
  }

  public ngOnInit() {
    this.getOrders();
  }

  public onPageChange(page: number): void {
    this.currentParams.page = page;
    this.getOrders();
  }

  private getOrders(): void {
    this.isLoading = true;

    this.ordersService
      .getOrdersForUser(this.currentParams)
      .subscribe((orders: Collection<Order>) => {
        this.orders = orders.members;
        this.totalItems = orders.totalItems;
        this.isLoading = false;
      });
  }
}
