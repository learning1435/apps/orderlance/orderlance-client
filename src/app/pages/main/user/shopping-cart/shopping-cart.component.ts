import {Component, OnInit} from '@angular/core';
import {OrdersService, ShoppingCartService} from '../../../../@services';
import {CartItem, MakeOrder, Order} from '../../../../@models';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {ConfirmDialogComponent, PaymentMethodComponent} from '../../../../@theme/components';
import {PaymentMethod} from '../../../../@core/enums';
import {Router} from '@angular/router';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent implements OnInit {

  public cartItems: CartItem[];

  constructor(
    private shoppingCartService: ShoppingCartService,
    private dialogService: NbDialogService,
    private router: Router,
    private ordersService: OrdersService,
  ) {
  }

  public ngOnInit() {
    this.cartItems = this.shoppingCartService.getCartItems();
  }

  public order(): void {
    const ref: NbDialogRef<PaymentMethodComponent> = this.dialogService
      .open(PaymentMethodComponent, {
        context: {
          confirmFn: (method: PaymentMethod): void => {
            ref.close();
            this.handleOrderByMethod(method);
          },
          cancelFn: (): void => {
            ref.close();
          },
        },
      });
  }

  public clearCart(): void {
    const ref: NbDialogRef<ConfirmDialogComponent> = this.dialogService
      .open(ConfirmDialogComponent, {
        context: {
          message: 'Do you want to clear your list?',
          confirmFn: (): void => {
            ref.close();

            this.clearList();
          },
          cancelFn: (): void => {
            ref.close();
          },
        },
      });
  }

  public getTotal(): number {
    let total: number = 0;

    this.cartItems.map((item: CartItem) => {
      total += item.count * item.meal.price;
    });

    return total;
  }

  private handleOrderByMethod(method: PaymentMethod): void {
    const request: MakeOrder = {
      meals: this.cartItems.map((item: CartItem) => ({
        count: item.count,
        meal: item.meal,
      })),
      paymentMethod: method,
    };

    this.ordersService
      .makeOrder(request)
      .subscribe(
        (order: Order) => {
          this.clearList();
          this.router.navigate(['/orders'])
            .then(() => {
              const ref: NbDialogRef<ConfirmDialogComponent> = this.dialogService
                .open(ConfirmDialogComponent, {
                  context: {
                    message: `We have your order with ID ${order.id}. We'll try prepare it ASAP ;)`,
                    cancelLabel: null,
                    confirmLabel: 'Ok',
                    confirmFn: (): void => {
                      ref.close();
                    },
                  },
                });
            });
        },
      );
  }

  private clearList(): void {
    this.cartItems = [];
    this.shoppingCartService.clear();
  }
}
