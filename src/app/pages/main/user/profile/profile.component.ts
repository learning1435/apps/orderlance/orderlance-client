import {Component, OnInit} from '@angular/core';
import {UserData} from '../../../../@services';
import {User} from '../../../../@models';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {ChangePasswordComponent, ChangeProfileComponent} from '../../../../@theme/components';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  public user: User = null;

  constructor(
    private userData: UserData,
    private dialogService: NbDialogService,
  ) {
  }

  public ngOnInit() {
    this.userData
      .getUser()
      .subscribe((user: User) => {
        this.user = user;
      });
  }

  public changeProfile(): void {
    const ref: NbDialogRef<ChangeProfileComponent> = this.dialogService.open(ChangeProfileComponent, {
      context: {
        closeFn: (): void => {
          ref.close();
        },
      },
    });
  }

  public changePassword(): void {
    const ref: NbDialogRef<ChangePasswordComponent> = this.dialogService.open(ChangePasswordComponent, {
      context: {
        closeFn: (): void => {
          ref.close();
        },
      },
    });
  }

  public getAvatar(): string {
    return `/assets/images/avatars/${this.user.avatar}.svg`;
  }
}
