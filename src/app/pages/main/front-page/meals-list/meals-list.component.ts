import {Component, Input, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {MealsService} from '../../../../@services';
import {Collection, CollectionParams, Meal} from '../../../../@models';
import {LIST_ANIMATION} from '../../../../@theme/animations';

@Component({
  selector: 'app-meals-list',
  templateUrl: './meals-list.component.html',
  styleUrls: ['./meals-list.component.scss'],
  animations: LIST_ANIMATION,
})
export class MealsListComponent implements OnInit {

  @Input() categorySelector: Subject<string>;

  public currentParams: CollectionParams = {
    page: 1,
    itemsPerPage: 10,
  };
  public meals: Meal[] = [];
  public isLoading: boolean = false;
  public totalItems: number = 0;
  public categoryId: string = null;

  constructor(
    private mealsService: MealsService,
  ) {
  }

  public ngOnInit(): void {
    this.getMeals(null);

    this.categorySelector
      .subscribe((selectedCategory) => {
        this.getMeals(selectedCategory);
      });
  }

  public onPageChange(page: number): void {
    this.currentParams.page = page;
    this.getMeals(this.categoryId);
  }

  private getMeals(categoryId: string = null): void {
    this.isLoading = true;
    this.categoryId = categoryId;

    this.mealsService
      .getMeals(this.currentParams, categoryId)
      .subscribe((meals: Collection<Meal>) => {
        this.meals = meals.members;
        this.totalItems = meals.totalItems;
        this.isLoading = false;
      });
  }
}
