import {Component, Input, OnInit} from '@angular/core';
import {Meal} from '../../../../../@models';
import {ShoppingCartService} from '../../../../../@services';

@Component({
  selector: 'app-meal-item',
  templateUrl: './meal-item.component.html',
  styleUrls: ['./meal-item.component.scss'],
})
export class MealItemComponent implements OnInit {

  @Input() meal: Meal;

  public count: number;

  constructor(
    private shoppingCartService: ShoppingCartService,
  ) {
  }

  public ngOnInit(): void {
    this.count = this.shoppingCartService.getCountForMeal(this.meal.id);
  }

  public addToCart(): void {
    this.shoppingCartService.addItems({
      count: this.count,
      meal: Object.assign({}, this.meal),
    });
  }

  public onCountChange(count: number): void {
    this.count = count;
  }
}
