import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {CategoriesService} from '../../../../@services';
import {Category, Collection} from '../../../../@models';

@Component({
  selector: 'app-category-selector',
  templateUrl: './category-selector.component.html',
  styleUrls: ['./category-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategorySelectorComponent implements OnInit {

  @Input() categorySelector: Subject<string>;

  public currentSelectedCategory: string = null;
  public categories: Category[] = [];

  constructor(
    private categoriesService: CategoriesService,
    private changeDetector: ChangeDetectorRef,
  ) {
  }

  public ngOnInit(): void {
    this.getCategories();
  }

  public selectCategory(categoryId: string): void {
    if (this.currentSelectedCategory === categoryId) {
      this.currentSelectedCategory = null;
    } else {
      this.currentSelectedCategory = categoryId;
    }

    this.categorySelector.next(this.currentSelectedCategory);
    this.changeDetector.detectChanges();
  }

  private getCategories(): void {
    this.categoriesService
      .getCategories({itemsPerPage: null, page: 1})
      .subscribe(
        (collection: Collection<Category>) => {
          this.categories = collection.members;
          this.changeDetector.detectChanges();
        },
      );
  }
}
