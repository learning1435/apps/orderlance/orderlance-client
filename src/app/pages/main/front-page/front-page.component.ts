import {Component} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.scss'],
})
export class FrontPageComponent {

  public categorySelector: Subject<string> = new Subject<string>();

}
