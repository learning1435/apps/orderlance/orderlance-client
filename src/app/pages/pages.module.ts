import {NgModule} from '@angular/core';
import {
  NbAccordionModule,
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule, NbIconModule, NbInputModule, NbListModule,
  NbMenuModule,
  NbPopoverModule,
  NbSpinnerModule,
  NbTooltipModule,
} from '@nebular/theme';
import {ThemeModule} from '../@theme/theme.module';
import {PagesComponent} from './pages.component';
import {PagesRoutingModule} from './pages-routing.module';
import {FrontPageComponent} from './main/front-page/front-page.component';
import {CategorySelectorComponent} from './main/front-page/category-selector/category-selector.component';
import {MealsListComponent} from './main/front-page/meals-list/meals-list.component';
import {MealItemComponent} from './main/front-page/meals-list/meal-item/meal-item.component';
import {FormsModule} from '@angular/forms';
import {ShoppingCartComponent} from './main/user/shopping-cart/shopping-cart.component';
import {PagerModule} from 'ng2-smart-table/components/pager/pager.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {AdminComponent} from './admin/admin.component';
import {DashboardComponent} from './admin/dashboard/dashboard.component';
import {CategoriesManagerComponent} from './admin/categories/categories-manager.component';
import {MealsManagerComponent} from './admin/meals/meals-manager.component';
import {OrdersManagerComponent} from './admin/orders/orders-manager.component';
import {UsersManagerComponent} from './admin/users/users-manager.component';
import {OrderProcessingComponent} from './main/order-processing/order-processing.component';
import {OrdersComponent} from './main/user/orders/orders.component';
import {ProfileComponent} from './main/user/profile/profile.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbCardModule,
    NbPopoverModule,
    NbTooltipModule,
    NbSpinnerModule,
    NbButtonModule,
    FormsModule,
    NbListModule,
    NbAlertModule,
    PagerModule,
    NgxPaginationModule,
    NbActionsModule,
    NbInputModule,
    NbAccordionModule,
    NbIconModule,
  ],
  declarations: [
    PagesComponent,
    FrontPageComponent,
    CategorySelectorComponent,
    MealsListComponent,
    MealItemComponent,
    ShoppingCartComponent,
    AdminComponent,
    DashboardComponent,
    CategoriesManagerComponent,
    MealsManagerComponent,
    OrdersManagerComponent,
    UsersManagerComponent,
    OrderProcessingComponent,
    OrdersComponent,
    ProfileComponent,
  ],
})
export class PagesModule {
}
