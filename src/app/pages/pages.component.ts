import {Component} from '@angular/core';
import {ROUTES_ANIMATION} from '../@theme/animations';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
  animations: ROUTES_ANIMATION,
})
export class PagesComponent {
}
